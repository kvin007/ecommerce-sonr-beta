export class Product {

    constructor(id: number, name: string, description: string,
        price: number, image: string, category_id: number, category_name: string) {
        this._id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.image = image;
        this.category_id = category_id;
        this.category_name = category_name;
    }

    _id: number;
    name: string;
    description: string;
    price: number;
    image: string;
    category_id: number;
    category_name: string;

}