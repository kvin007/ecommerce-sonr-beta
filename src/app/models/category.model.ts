export class Category { 

    constructor(id:number,name: string, description:string){
        this._id=id;
        this.name=name;
        this.description=description;
    }

    _id: number;
    name: string;
    description: string;
}