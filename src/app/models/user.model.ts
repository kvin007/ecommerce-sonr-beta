export class User { 

    constructor(id:number,username: string, password:string){
        this._id=id;
        this.username=username;
        this.password=password;
    }

    _id: number;
    username: string;
    password: string;
}