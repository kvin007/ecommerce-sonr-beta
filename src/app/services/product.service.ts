import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})

export class ProductService {

  uri = 'http://localhost:8010';

  constructor(private http: HttpClient) { }

  getProducts() {
    return this.http.get<Product[]>(`${this.uri}/products`);
  };

  getProductById(id) {
    return this.http.get<Product>(`${this.uri}/products/${id}`);
  };

  addProduct(name, description, price, image,category_id,category_name) {
    const product = {
      name: name,
      description: description,
      price: price,
      image: image,
      category_id : category_id,
      category_name:category_name,
    }
    return this.http.post(`${this.uri}/products/`, product);
  }

  updateProduct(id, name, description, price, image,category_id,category_name) {
    const product = {
      name: name,
      description: description,
      price: price,
      image: image,
      category_id : category_id,
      category_name:category_name,
    }
    return this.http.put(`${this.uri}/products/${id}`, product);
  }

  deleteProduct(id) {
    return this.http.delete(`${this.uri}/products/${id}`);
  }

}
