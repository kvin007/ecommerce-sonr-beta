import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  uri = 'http://localhost:8010';

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<User[]>(`${this.uri}/users`);
  };

  getUserById(id) {
    return this.http.get<User>(`${this.uri}/users/${id}`);
  };

  getUserByUsernameAndPassword(username,password){
    const user = {
      username: username,
      password: password
    }
    return this.http.post<AuthResponse>(`${this.uri}/users/find/`,user);
  }

  addUser(username , password) {
    const user = {
      username: username,
      password: password
    }
    return this.http.post(`${this.uri}/users/`, user);
  }

  updateUser(id, username, password) {
    const user = {
      username: username,
      password: password
    }
    return this.http.put(`${this.uri}/users/${id}`, user);
  }

  deleteUser(id) {
    return this.http.delete(`${this.uri}/users/${id}`);
  }

}

interface AuthResponse {
  permission : boolean;
}
