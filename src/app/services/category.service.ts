import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  uri = 'http://localhost:8010';

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get<Category[]>(`${this.uri}/categories`);
  };

  getCategoryById(id) {
    return this.http.get<Category>(`${this.uri}/categories/${id}`);
  };

  addCategory(name, description) {
    const category = {
      name: name,
      description: description
    }
    return this.http.post(`${this.uri}/categories/`, category);
  }

  updateCategory(id, name, description) {
    const category = {
      name: name,
      description: description
    }
    return this.http.put(`${this.uri}/categories/${id}`, category);
  }

  deleteCategory(id) {
    return this.http.delete(`${this.uri}/categories/${id}`);
  }

}
