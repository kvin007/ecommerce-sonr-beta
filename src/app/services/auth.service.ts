import { Injectable } from '@angular/core';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private userService:UserService) { }
  
  public loggedIn = false;

  isLoggedIn(){
    return this.loggedIn;
  }
  setLoggedIn(boolean){
    this.loggedIn = boolean;
  }

  getUserDetails(username,password){
    return this.userService.getUserByUsernameAndPassword(username,password);
  }

}
