import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';

import { ProductService } from '../../services/product.service';
import { CategoryService } from '../../services/category.service';
import { UserService } from '../../services/user.service';
import { Product } from '../../models/product.model';
import { Category } from '../../models/category.model';
import { User } from '../../models/user.model';


import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  currentTable: string;
  name: string;
  showProducts: boolean = false;
  showCategories: boolean = false;
  showUsers: boolean = false;
  products: Product[];
  categories: Category[];
  users:User[];
  tableColumns: string[];

  newProduct: Product;
  newCategory: Category;
  newUser: User;

  closeResult: string;
  currentElementName: string;

  constructor(public route: ActivatedRoute,
    private productService: ProductService,
    private categoryService: CategoryService,
    private userService: UserService,
    private modalService: NgbModal) { }

  ngOnInit() {

    this.currentTable = this.route.snapshot.url[0].path.split("/").pop();
    this.fetchData();

  }

  fetchData() {
    if (this.currentTable === "products") this.updateTableToProducts();
    else if (this.currentTable === "categories") this.updateTableToCategories();
    else if (this.currentTable === "users") this.updateTableToUsers();
  }

  updateTableToProducts() {
    this.name = "Productos";
    this.showProducts = true;
    this.showCategories = false;
    this.showUsers = false;
    this.tableColumns = ['Nombre', 'Descripción', 'Categoría', 'Precio', 'Acciones'];
    this.productService.getProducts().subscribe((products) => {
      this.products = products;
    });
    this.categoryService.getCategories().subscribe((categories) => {
      this.categories = categories;
    });
  }

  updateTableToCategories() {
    this.name = "Categorías";
    this.showProducts = false;
    this.showCategories = true;
    this.showUsers = false;
    this.tableColumns = ['Nombre', 'Descripción', 'Acciones'];
    this.categoryService.getCategories().subscribe((categories) => {
      this.categories = categories;
    });
  }

  updateTableToUsers() {
    this.name = "Usuarios";
    this.showProducts = false;
    this.showCategories = false;
    this.showUsers = true;
    this.tableColumns = ['Usuario', 'Contraseña', 'Acciones'];
    this.userService.getUsers().subscribe((users) => {
      this.users = users;
    });
  }


  //// Modals for products

  // used by bootstrap modal
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // FOR PRODUCTS

  // Opens modals
  openEditProduct(item, content) {  // Applies for create too
    if (this.showProducts) this.newProduct = Object.assign({}, item);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openDeleteProduct(prod, content) {
    this.newProduct = Object.assign({}, prod);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  // Methods to add, edit, delete a specific product
  saveProduct() {
    this.newProduct.category_id = this.categories.find((cat) => cat.name === this.newProduct.category_name)._id;
    this.productService.addProduct(this.newProduct.name, this.newProduct.description,
      this.newProduct.price, this.newProduct.image, this.newProduct.category_id,
      this.newProduct.category_name).subscribe(() => {
        this.modalService.dismissAll();
        this.fetchData();
      });
  }

  editProduct() {
    this.newProduct.category_id = this.categories.find((cat) => cat.name === this.newProduct.category_name)._id;
    this.productService.updateProduct(this.newProduct._id, this.newProduct.name, this.newProduct.description,
      this.newProduct.price, this.newProduct.image, this.newProduct.category_id,
      this.newProduct.category_name).subscribe(() => {
        this.modalService.dismissAll();
        this.fetchData();
      });
  }

  deleteProduct() {
    this.productService.deleteProduct(this.newProduct._id).subscribe(() => {
      this.modalService.dismissAll();
      this.fetchData();
    });
  }

  // FOR CATEGORIES

  // Opens modals
  openEditCategory(item, content) {  // Applies for create too
    if (this.showCategories) this.newCategory = Object.assign({}, item);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openDeleteCategory(cat, content) {
    this.newCategory = Object.assign({}, cat);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // Methods to add, edit, delete a specific product
  saveCategory() {
    this.categoryService.addCategory(this.newCategory.name, this.newCategory.description).subscribe(() => {
      this.modalService.dismissAll();
      this.fetchData();
    });
  }

  editCategory() {
    this.categoryService.updateCategory(this.newCategory._id, this.newCategory.name, this.newCategory.description)
      .subscribe(() => {
        this.modalService.dismissAll();
        this.fetchData();
      });
  }

  deleteCategory() {
    this.categoryService.deleteCategory(this.newCategory._id).subscribe(() => {
      this.modalService.dismissAll();
      this.fetchData();
    });
  }


  // FOR USER

  // Opens modals
  openEditUser(item, content) {  // Applies for create too
    if (this.showUsers) this.newUser = Object.assign({}, item);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openDeleteUser(user, content) {
    this.newUser = Object.assign({}, user);
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // Methods to add, edit, delete a specific product
  saveUser() {
    this.userService.addUser(this.newUser.username, this.newUser.password).subscribe(() => {
      this.modalService.dismissAll();
      this.fetchData();
    });
  }

  editUser() {
    this.userService.updateUser(this.newUser._id, this.newUser.username, this.newUser.password)
      .subscribe(() => {
        this.modalService.dismissAll();
        this.fetchData();
      });
  }

  deleteUser() {
    this.userService.deleteUser(this.newUser._id).subscribe(() => {
      this.modalService.dismissAll();
      this.fetchData();
    });
  }


}
