import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router,private authService:AuthService) { }

  ngOnInit() {
  }

  onLoadAdmin(event){
    event.preventDefault();
    const target = event.target;
    const username = target.querySelector('#inputUser').value;
    const password = target.querySelector('#inputPassword').value;

    this.authService.getUserDetails(username,password).subscribe(data => {
      console.log(data);
      console.log(data.permission);
      if (data.permission){
        this.router.navigate(['/admin/products']);
        this.authService.setLoggedIn(true);
      }else{
        alert('Invalid credentials');
      }
    })
  }

}
