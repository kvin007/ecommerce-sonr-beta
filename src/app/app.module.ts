import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { ProductService } from './services/product.service';
import { CategoryService } from './services/category.service';

import { AppComponent } from './app.component';

import { ShoppingComponent } from './shopping/shopping.component';
import { FilterComponent } from './shopping/filter/filter.component';
import { HeaderComponent } from './shopping/header/header.component';
import { ProductComponent } from './shopping/product/product.component';
import { ProductListComponent } from './shopping/product-list/product-list.component';
import { ProductCardComponent } from './shopping/product-list/product-card/product-card.component';

import { LoginComponent } from './login/login.component';

import { AdminComponent } from './admin/admin.component';
import { SidebarComponent } from './admin/sidebar/sidebar.component';
import { TableComponent } from './admin/table/table.component';
import { AuthGuardService } from './services/auth-guard.service';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';


const routes: Routes = [
  { path: 'login', component: LoginComponent },

  {
    path: 'shopping', component: ShoppingComponent,
    children: [
      { path: 'products', component: ProductListComponent },
      { path: 'products/detail/:id', component: ProductComponent },
      { path: 'category/:id', component: ProductListComponent }
    ]
  },

  {
    path: 'admin', canActivate: [AuthGuardService], component: AdminComponent,
    children: [
      { path: 'products', component: TableComponent },
      { path: 'categories', component: TableComponent },
      { path: 'users', component: TableComponent }
    ]
  },

  { path: '', redirectTo: 'shopping/products', pathMatch: 'full' }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FilterComponent,
    ProductComponent,
    ProductListComponent,
    ProductCardComponent,
    SidebarComponent,
    LoginComponent,
    ShoppingComponent,
    AdminComponent,
    TableComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ProductService, CategoryService, UserService, AuthGuardService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
