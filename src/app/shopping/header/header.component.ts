import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router,private authService:AuthService) { }

  ngOnInit() {
  }
  onLoadLogin(){
    if (this.authService.isLoggedIn()){
      this.router.navigate(['/admin/products'])
    }else{
      this.router.navigate(['/login']);
    }

  }

}
