import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[];
  currentRoute: string;
  findCat: string;
  id: string;

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.currentRoute = this.route.snapshot.url[0].path;
    if (this.route.snapshot.url.length>1){
      this.findCat = this.route.snapshot.url[1].path;
    }

    this.route.url.subscribe((params) => {
      this.currentRoute = params[0].path;
      if (params.length>1){
        this.findCat = params[1].path;
      }
      // console.log(this.currentRoute);
      // console.log(this.findCat);
    });

    this.fetchData();


  }
  fetchData(){
    this.productService.getProducts().subscribe((products: Product[]) => {
      this.products = products;
      if (this.currentRoute !== "products") {
        // console.log(this.products);
        this.products = this.products.filter(prod => prod.category_id.toString() == this.findCat);
      }
    });
  }
}
