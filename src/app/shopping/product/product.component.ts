import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {

  product: Product;
  id: any;


  constructor(private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService) { }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];

    this.productService.getProductById(this.id).subscribe((product) => {
      this.product = product;
    });

    this.route.params.subscribe((params) => {
      this.id = params['id']
    });

  }

}
